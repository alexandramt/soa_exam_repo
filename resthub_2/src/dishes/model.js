var mongoose = require('mongoose');
// Setup schema
var dishSchema = mongoose.Schema({
    name: String,
    imageUrl: String,
    price: Number,
    portions: Number
});
var Dish = module.exports = mongoose.model('dish', dishSchema);
module.exports.get = function (callback, limit) {
    Dish.find(callback).limit(limit);
}