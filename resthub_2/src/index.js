let express = require('express');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let app = express();

let apiRoutes = require("./dishes/routes");

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

let allowCrossDomain = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,POST");
    res.header("Access-Control-Allow-Headers", "Content-Type");

    next();
};

app.use(allowCrossDomain);

mongoose.connect('mongodb://10.0.75.1/soa_project');
var db = mongoose.connection;


var port = process.env.PORT || 8081;

app.get('/', (req, res) => res.send('Hello dishes API'));

app.use('/api', apiRoutes);

app.listen(port, function () {
    console.log("Running Dish API on port which port?" + port);
});

