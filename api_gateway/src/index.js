var express = require('express');
var app = express();
var router = require('./routers/router');
var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


let allowCrossDomain = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,POST");
    res.header("Access-Control-Allow-Headers", "Content-Type");

    next();
};
app.use(allowCrossDomain);

app.get('/', (req, res) => {
    res.send("Working API Gateway")
});

app.use(router);

app.listen(3000);