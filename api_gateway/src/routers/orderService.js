var express = require('express');
var router = express.Router();
const apiAdapter = require('./apiAdapter');

// const BASE_URL = 'http://localhost:8080/api';
const BASE_URL = 'http://10.0.75.1:49161/api';
const api = apiAdapter(BASE_URL);

router.post('/orders', (req, res) => {
    api.post(req.path, req.body).then(resp => {
        res.send(resp.data)
    })
});

module.exports = router