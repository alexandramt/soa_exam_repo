var express = require('express');
var router = express.Router();
const apiAdapter = require('./apiAdapter');

// const BASE_URL = 'http://localhost:8081/api';
const BASE_URL = 'http://10.0.75.1:49160/api';

const api = apiAdapter(BASE_URL);

router.get('/dishes', (req, res) => {
    console.log(req);
    api.get(req.path).then(resp => {
        res.send(resp.data)
    })
});

module.exports = router;