var express = require('express');
var router = express.Router();

var dishService = require('./dishService');
var orderService = require('./orderService');

router.use((req, res, next) => {
    console.log("Called: ", req.path)
    next()
});

router.use(dishService);
router.use(orderService);

module.exports = router;