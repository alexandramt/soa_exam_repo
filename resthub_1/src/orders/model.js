var mongoose = require('mongoose');
// Setup schema
var orderSchema = mongoose.Schema({
    dishes : [],
    address: String,
    date: {
        type: Date,
        default: Date.now()
    },
    time: String,
    amount: Number
});
var Order = module.exports = mongoose.model('order', orderSchema);

module.exports.get = function (callback, limit) {
    Order.find(callback).limit(limit);
};