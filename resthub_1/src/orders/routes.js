let router = require('express').Router();

router.get('/', function (req, res) {
    res.json({
        status: 'API Working',
        message: 'Order API working',
    });
});
var orderController = require('./controller');
// Contact routes
router.route('/orders')
    .get(orderController.index)
    .post(orderController.new);
router.route('/orders/:order_id')
    .get(orderController.view)
    .patch(orderController.update)
    .put(orderController.update)
    .delete(orderController.delete);

// Export API routes
module.exports = router;