export interface IDish {
  name: string;
  imageUrl: string;
  price: number;
  portions: number;
}

export class Dish implements IDish{
  name: string;
  imageUrl: string;
  price: number;
  portions: number;
  quantity: number;

  constructor(item: IDish) {
    this.name = item.name ? item.name : '';
    this.imageUrl = item.imageUrl ? item.imageUrl : '';
    this.price = item.price ? item.price : 0;
    this.portions = item.portions ? item.portions : 0;
    this.quantity = 0;
  }
}
